FROM registry.cto.ai/official_images/bash:2-buster-slim

WORKDIR /ops

RUN apt update && apt install -y git

ADD checker.sh .
