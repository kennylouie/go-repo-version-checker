# go-repo-version-checker op

This @cto.ai/op checks out a golang git repo and checks if the current branch's go version is higher than the origin/master version.

This op can be run in the cli (using the @cto.ai tool https://cto.ai/docs/getting-started) or over slack (with the @cto.ai slackbot https://cto.ai/docs/slackapp)

# To run op:

```{bash}
ops run @kennylouie/go-repo-version-checker:${__LATEST__VERSION__} --repo ${__REPO__} --branch ${__BRANCH__}
```

# Building from source and running

+ clone this repo
+ ensure @cto.ai/ops is installed
+ build the op
```
ops build .
```

```
ops run .
```
