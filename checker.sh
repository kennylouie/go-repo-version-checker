#!/bin/bash

set -u

main()
{
	# send a track event
	need_cmd curl

	curl --request POST --header "Content-Type: application/json" http://127.0.0.1:$SDK_SPEAK_PORT/track --data '{"tags": ["github", "sdk-go", "tests", "go-repo-version-checker"], "event": "tests", "message": "starting op from inside gh actions workflow"}'

	need_cmd ctoai
	need_cmd git

	local _repo _br
	get_repo
	get_branch

	ensure cd "${SDK_STATE_DIR}"
	ensure git clone "${_repo}" tmp && cd tmp && git checkout "${_br}"

	local _current_tag _semver_current_tag _master_tag _semver_master_tag
	get_versions
	is_version_higher

	say Done!
}

get_repo()
{
	_repo="$(ctoai prompt input \
		--message "Remote repository URL" \
		--name "repo")"

	check_empty "${_repo}"
}

get_branch()
{
	_br="$(ctoai prompt input \
		--message "branch" \
		--name "branch" \
		--default "master")"

	check_empty "${_br}"
}

get_versions()
{
	_current_tag="$(git describe --tags)"
	_semver_current_tag=${_current_tag%%-*}

	say "current: ${_semver_current_tag}"

	_master_tag="$(git fetch && git describe origin/master --tags)"
	_semver_master_tag=${_master_tag%%-*}

	say "master: ${_semver_master_tag}"
}

is_version_higher()
{
	[ "$_semver_current_tag" = "$(echo -e "$_semver_current_tag\n$_semver_master_tag" | sort -V | head -n1)" ] && err "version not higher!"
}

ensure()
{
	if ! "$@"; then err "command failed: $*"; fi
}

check_empty()
{
	[ -n "$1" ] || err "cannot be empty"
}

# utility function to indicate to user that program is needed
need_cmd()
{
	if ! check_cmd "$1"; then
		err "need $1 (command not found)"
	fi
}

# utility function to check if a program exectuable is callable
check_cmd()
{
	type "$1" >/dev/null 2>&1
}

# utility function to echo error and exit program
err()
{
	say "$@" >&2
	exit 1
}

# utility function to print with a tagline
say()
{
	ctoai print "$(printf "%b" "@checkout: $@\n")"
}

main "$@"
